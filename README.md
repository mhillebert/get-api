# Quick 'n' Easy gatewayconfigapi
##Setup
1. Install `vagrant`, and the vagrant plugins by running: `vagrant plugin install vagrant-bindfs`, `vagrant plugin install vagrant-hostsupdater`
2. cd into this directory (location of this file)
3 run `vagrant up`
4 when done, run `vagrant halt`

To remove: run `vagrant destroy`



This should download the box, add `192.168.33.10 gatewayconfigapi` to your host file,
mount the shred folder,  
and install and run docker containers within the vm.

##Create a new api endpoint
This simplistic api returns the contents of json files. Wherever you place this json file within 
the `sites/web/web/` directory will determine its url.

[Optional]
Add documentation to the index.html for your new endpoint.

For example:
To create an endpoint for `http://gatewayconfigapi/users/all`, you would create:

```
-sites
 |- html
 |- web 
    |- web
     |- users  [NEW DIRECTORY]
     |  |- all.json [NEW FILE]
     |
     |- app.php
     |- index.html
```

And if your all.json file contained:
```
{
 "users": [
   {
    "name": "Ace",
    "email": "ace@bitbucket.org"
   },
   {
    "name": "Bruce",
    "email": "bruce@bitbucket.org"
   }
 ]
}
```
Then your request would receive:
`{"users": [{"name": "Ace", "email": "ace@bitbucket.org"}, {"name": "Bruce", "email": "bruce@bitbucket.org"}]}`

