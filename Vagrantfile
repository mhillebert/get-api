# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

    config.vm.box = "bento/ubuntu-16.04"
    config.vm.network "private_network", ip: "192.168.33.10"
    #config.ssh.insert_key = true
    #config.ssh.forward_agent = true
    #config.ssh.username = 'vagrant'
    #config.ssh.password = 'vagrant'
    config.vm.hostname = "gatewayconfigapi"
    config.vm.synced_folder "./docker", "/vagrant_data/docker", :create=>true, :nfs => { :mount_options => ["dmode=777","fmode=666"] }
    config.vm.synced_folder "./sites", "/vagrant_data/sites", :create=>true, :nfs => { :mount_options => ["dmode=777","fmode=666"] }


    config.vm.provider :virtualbox do |vb|
          vb.name = "gatewayconfigapi"
          vb.memory = 4096
          vb.cpus = 4
          vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
          vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    end
    config.vm.provision "shell", inline: <<-SHELL
        export DEBIAN_FRONTEND=noninteractive
        rm /vagrant/vm_build.log
        echo -e "### You can see details of the build by monitoring ./vm_build.log file."

        echo -e "Updating box..."
        apt-get update -qq >> /vagrant/vm_build.log 2>&1

        echo -e "Installing docker pre-reqs..."
        apt-get install -qq apt-transport-https ca-certificates curl software-properties-common >> /vagrant/vm_build.log 2>&1

        echo -e "Adding docker repository..."
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - >> /vagrant/vm_build.log 2>&1
        add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" >> /vagrant/vm_build.log 2>&1

        echo -e "Updating repository list..."
        apt-get update -qq >> /vagrant/vm_build.log 2>&1

        echo -e "Installing docker"
        apt-get install -qq docker-ce docker-compose >> /vagrant/vm_build.log 2>&1

        echo -e "Adding docker user..."
        usermod -a -G docker vagrant >> /vagrant/vm_build.log 2>&1
        sudo ln -s /vagrant_data/docker/graceful_shutdown.sh /etc/rc0.d/k99stop_vm >> /vagrant/vm_build.log 2>&1
        sudo ln -s /vagrant_data/docker/graceful_shutdown.sh /etc/rc6.d/k99stop_vm >> /vagrant/vm_build.log 2>&1
        sudo chmod a+x /vagrant_data/docker/graceful_shutdown.sh >> /vagrant/vm_build.log 2>&1

        echo -e "Creating docker network..."
        docker network create nginx-proxy >> /vagrant/vm_build.log 2>&1

        echo -e "Building basephp docker image..."
        cd /vagrant_data/docker/basephp;
        docker build . -t basephp >> /vagrant/vm_build.log 2>&1

        echo -e "Building and starting nginx-proxy container..."
        cd /vagrant_data/docker/nginx-proxy;
        docker-compose up -d >> /vagrant/vm_build.log 2>&1

        echo -e "Building and starting web container..."
        cd /vagrant_data/docker/web;
        docker-compose up -d >> /vagrant/vm_build.log 2>&1

        # bash aliases for easy docker container access
        echo -e "Adding nice things:"

        echo 'alias sshnginxproxy="docker exec -it nginx-proxy /bin/bash"' >> /home/vagrant/.bashrc
        echo -e " sshnginxproxy will ssh into nginx-proxy container"

        echo 'alias sshnginxweb="docker exec -it web_nginx_web_1 /bin/bash"' >> /home/vagrant/.bashrc
        echo -e " sshnginxweb will ssh into nginx-web container"

        echo 'alias sshphpweb="docker exec -it web_php_web_1 /bin/bash"' >> /home/vagrant/.bashrc
        echo -e " sshphpweb will ssh into php-web container"

        #echo "127.0.0.1 php_web" | tee -a /etc/hosts

    SHELL

    config.vm.provision :shell, :inline => "cd /vagrant_data/docker/nginx-proxy/;docker-compose up -d", run: "always"
    config.vm.provision :shell, :inline => "cd /vagrant_data/docker/web/;docker-compose up -d", run: "always"
end
