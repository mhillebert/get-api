<?php
const INDEX = '<h1>gatewayconfigapi</h1><p>Available endpoints:</p>';
switch ($_SERVER['REQUEST_URI'])
{
    case $_SERVER['REQUEST_URI'] === '/':
        $page = INDEX . getEndpointList();
        break;
    default:
        try {
            $page =  @file_get_contents('./'.$_SERVER['REQUEST_URI'].'.json');

            // dum config api stuff follows:
            $page = json_encode([ "Value" => $page]);


        }
        catch(\Exception $e)
        {
            // nothing to do
        }
}

if (!$page) {
    http_response_code(404);
    $page = INDEX . getEndpointList();
}

echo $page;


function getEndpointList() {
    $files = rsearch("/var/www/web", "/.*\.json/");
    $return = "<ul>";
    foreach ($files as $file) {
        $return .= "<li><a href='{$file}'>{$file}</a></li>";
    }

    $return .= "</ul>";
    return $return;
}



function rsearch($folder, $pattern) {
    $dir = new RecursiveDirectoryIterator($folder);
    $ite = new RecursiveIteratorIterator($dir);
    $files = new RegexIterator($ite, $pattern, RegexIterator::GET_MATCH);
    $fileList = array();
    foreach($files as $file) {
        $urlFilename = substr($file[0],12, -5);
        $fileList = array_merge($fileList, [$urlFilename]);
    }
    return $fileList;
}



